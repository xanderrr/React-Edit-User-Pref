FROM node:15.14.0

EXPOSE 3000
EXPOSE 3001

WORKDIR /app
COPY ./edit-user-preferences .

RUN npm install
RUN npm run build


CMD ["npm", "start"]

			